/*
Purpose:
- Browser Printmaps Client in Javascript.

Description:
- Requires Browser with HTML5 support.

Releases:
- v0.1.0 - 2020/07/07 : initial release
- v0.1.1 - 2020/07/11 : add documentation
- v0.2.0 - 2020/07/19 : enable/disable buttons
- v0.3.0 - 2020/07/20 : load/save map ID

Author:
- Klaus Tockloth

Copyright and license:
- Copyright (c) 2020 Klaus Tockloth
- MIT license

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the Software), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

The software is provided 'as is', without warranty of any kind, express or implied, including
but not limited to the warranties of merchantability, fitness for a particular purpose and
noninfringement. In no event shall the authors or copyright holders be liable for any claim,
damages or other liability, whether in an action of contract, tort or otherwise, arising from,
out of or in connection with the software or the use or other dealings in the software.

Contact (eMail):
- printmaps.service@gmail.com

Remarks:
- NN

Links:
- NN
*/

// globale vars
var printmapsMapYAML = "";
var printmapsMapAttribues = {};
var printmapsURL = "";
var printmapsMapID = "";

/* ----------
saves map ID to file
 ---------- */
function printmapsSaveID() {
  // write file
  var blob = new Blob([printmapsMapID], { type: "text/plain;charset=utf-8" });
  saveAs(blob, "map.id");

  // disable button
  document.getElementById("mapActionID").disabled = true;
}

/* ----------
creates the meta data for a new map
 ---------- */
function printmapsCreateMap() {
  // prerequisite: YAML map file loaded and parsed
  if (printmapsMapYAML === "") {
    // show error status in GUI element title
    document.getElementById("titleCreateMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

    // show error info in GUI element text area
    document.getElementById("textCreateMapInfo").innerHTML = "YAML map file missing or invalid";

    return;
  }

  // build printmaps request
  var printmapsObject = {};
  printmapsObject.Data = {};
  printmapsObject.Data.Type = "maps";
  printmapsObject.Data.ID = "";
  printmapsObject.Data.Attributes = {};
  printmapsObject.Data.Attributes = printmapsMapAttributes;

  // 'Projection' is always a string
  printmapsObject.Data.Attributes.Projection = printmapsObject.Data.Attributes.Projection.toString();

  // 'UserFiles' is an internal object only for printmaps commandline client
  delete printmapsObject.Data.Attributes.UserFiles;

  // stringify JSON request
  var printmapsCreateMapRequestData = JSON.stringify(printmapsObject, undefined, 2);

  // save printmaps root URL
  printmapsURL = printmapsObject.Data.Attributes.ServiceURL;

  // build printmaps 'create map' URL
  var printmapsCreateMapURL = printmapsURL + "metadata";

  // send printmaps request
  fetch(printmapsCreateMapURL, {
    method: "POST",
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
      "Content-Type": "application/vnd.api+json; charset=utf-8",
    },
    body: printmapsCreateMapRequestData,
  })
    .then(function (response) {
      // show response status in GUI element text area
      document.getElementById("textCreateMapInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in GUI element title
      document.getElementById("titleCreateMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      // 'Data' not set: response is error info
      if (data.hasOwnProperty("Data")) {
        // save map ID
        printmapsMapID = data.Data.ID;

        // add map ID to GUI element title
        document.getElementById("titleCreateMapInfo").innerHTML += " - " + printmapsMapID + " (ID)";
      }

      // stringify JSON response
      var printmapsCreateMapResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textCreateMapInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsCreateMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

      // enable buttons
      document.getElementById("mapInputUpdate").disabled = false;
      document.getElementById("mapActionUpdate").disabled = false;
      document.getElementById("mapInputUpload").disabled = false;
      document.getElementById("mapActionUpload").disabled = false;
      document.getElementById("mapActionOrder").disabled = false;
      document.getElementById("mapActionStatus").disabled = false;
      document.getElementById("mapActionData").disabled = false;
      document.getElementById("mapActionDelete").disabled = false;
      document.getElementById("mapActionFetch").disabled = false;

      // disable buttons
      document.getElementById("mapInputID").disabled = true;
      document.getElementById("mapInputCreate").disabled = true;
      document.getElementById("mapActionCreate").disabled = true;
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleCreateMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textCreateMapInfo").innerHTML = error;
    });
}

/* ----------
updates the meta data of an existing map
---------- */
function printmapsUpdateMap() {
  // prerequisite: YAML map file loaded and parsed
  if (printmapsMapYAML === "") {
    // show error status in GUI element title
    document.getElementById("titleUpdateMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

    // show error info in GUI element text area
    document.getElementById("textUpdateMapInfo").innerHTML = "YAML map file missing or invalid";

    return;
  }

  // build printmaps request
  var printmapsObject = {};
  printmapsObject.Data = {};
  printmapsObject.Data.Type = "maps";
  printmapsObject.Data.ID = printmapsMapID;
  printmapsObject.Data.Attributes = {};
  printmapsObject.Data.Attributes = printmapsMapAttributes;

  // 'Projection' is always a string
  printmapsObject.Data.Attributes.Projection = printmapsObject.Data.Attributes.Projection.toString();

  // 'UserFiles' is an internal object only for printmaps commandline client
  delete printmapsObject.Data.Attributes.UserFiles;

  // stringify JSON request
  var printmapsUpdateMapRequestData = JSON.stringify(printmapsObject, undefined, 2);

  // save printmaps root URL
  printmapsURL = printmapsObject.Data.Attributes.ServiceURL;

  // build printmaps 'create map' URL
  var printmapsUpdateMapURL = printmapsURL + "metadata/patch/";

  // send printmaps request
  fetch(printmapsUpdateMapURL, {
    method: "POST",
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
      "Content-Type": "application/vnd.api+json; charset=utf-8",
    },
    body: printmapsUpdateMapRequestData,
  })
    .then(function (response) {
      // show response status in GUI element text area
      document.getElementById("textUpdateMapInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in GUI element title
      document.getElementById("titleUpdateMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      if (response.status === 200) {
        // enable buttons
        document.getElementById("mapInputUpload").disabled = false;
        document.getElementById("mapActionUpload").disabled = false;
        document.getElementById("mapActionOrder").disabled = false;
        document.getElementById("mapActionStatus").disabled = false;
        document.getElementById("mapActionData").disabled = false;
        document.getElementById("mapActionDelete").disabled = false;
        document.getElementById("mapActionFetch").disabled = false;
      }

      return response.json();
    })
    .then((data) => {
      // stringify JSON response
      var printmapsUpdateMapResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textUpdateMapInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsUpdateMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleUpdateMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textUpdateMapInfo").innerHTML = error;
    });
}

/* ----------
uploads user supplied file (only one per action)
---------- */
function printmapsUploadFile(input) {
  // build printmaps 'upload file' URL
  var printmapsUploadURL = printmapsURL + "upload/" + printmapsMapID;

  var xhr = new XMLHttpRequest();

  // show file upload progress
  xhr.upload.onprogress = function (e) {
    // calculate upload progress
    var progress = (e.loaded * 100) / e.total;

    // show upload progress in title of GUI element
    document.getElementById("titleUploadFileInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "Upload progress " + Math.round(progress) + "%";
  };

  // show file upload end
  xhr.upload.onload = function (e) {
    // show upload status in title of GUI element
    document.getElementById("titleUploadFileInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Upload done";

    // show info (hint) in text area of GUI element
    document.getElementById("textUploadFileInfo").innerHTML =
      "Hint:\n" +
      "Fetch 'Map Data' to show list of already uploaded files.\n" +
      "See content of response element 'UserFiles' (name,size).";
  };

  // show upload status in title of GUI element
  document.getElementById("titleUploadFileInfo").innerHTML =
    new Date(Date.now()).toLocaleTimeString() + " - " + "Upload progress 0%";

  // start upload
  xhr.open("POST", printmapsUploadURL, true);
  xhr.send(new FormData(input.parentElement));
}

/* ----------
places a map build order
---------- */
function printmapsOrderMap() {
  // build printmaps request
  var printmapsObject = {};
  printmapsObject.Data = {};
  printmapsObject.Data.Type = "maps";
  printmapsObject.Data.ID = printmapsMapID;

  // stringify JSON request
  var printmapsOrderMapRequestData = JSON.stringify(printmapsObject, undefined, 2);

  // build printmaps 'order map' URL
  var printmapsOrderMapURL = printmapsURL + "mapfile";

  // send printmaps request
  fetch(printmapsOrderMapURL, {
    method: "POST",
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
      "Content-Type": "application/vnd.api+json; charset=utf-8",
    },
    body: printmapsOrderMapRequestData,
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textOrderMapInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleOrderMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      var printmapsOrderMapResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textOrderMapInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsOrderMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

      // disable download button
      document.getElementById("mapActionDownload").disabled = true;
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleOrderMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textOrderMapInfo").innerHTML = error;
    });
}

/* ----------
fetches the current build / processing state of the map
---------- */
function printmapsMapStatus() {
  // build printmaps 'map status' URL
  var printmapsMapStatusRequestURL = printmapsURL + "mapstate/" + printmapsMapID;

  // send printmaps request
  fetch(printmapsMapStatusRequestURL, {
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
    },
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textMapStatusInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleMapStatusInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      // stringify JSON response
      var printmapsMapStatusResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textMapStatusInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsMapStatusResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

      // show map build / processing state in title of GUI element
      if (data.Data.Attributes.MapBuildMessage !== "") {
        document.getElementById("titleMapStatusInfo").innerHTML += " - " + data.Data.Attributes.MapBuildMessage;
      } else if (data.Data.Attributes.MapBuildCompleted !== "") {
        document.getElementById("titleMapStatusInfo").innerHTML += " - " + "map build completed";
      } else if (data.Data.Attributes.MapBuildStarted !== "") {
        document.getElementById("titleMapStatusInfo").innerHTML += " - " + "map build started";
      } else if (data.Data.Attributes.MapOrderSubmitted !== "") {
        document.getElementById("titleMapStatusInfo").innerHTML += " - " + "map build submitted";
      } else if (data.Data.Attributes.MapMetadataWritten !== "") {
        document.getElementById("titleMapStatusInfo").innerHTML += " - " + "map metadata written";
      } else {
        document.getElementById("titleMapStatusInfo").innerHTML += " - " + "unknown status";
      }

      if (data.Data.Attributes.MapBuildSuccessful === "yes") {
        // enable download button
        document.getElementById("mapActionDownload").disabled = false;
      }
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleMapStatusInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textMapStatusInfo").innerHTML = error;
    });
}

/* ----------
downloads a successful build map
---------- */
function printmapsDownloadMap() {
  // build printmaps 'download' URL
  var printmapsDownloadMap = printmapsURL + "mapfile/" + printmapsMapID;

  // build HTML5 href (download) element
  var element = document.createElement("a");
  element.setAttribute("href", printmapsDownloadMap);
  element.setAttribute("download", "");
  element.setAttribute("type", "application/zip");

  // append new href element to DOM
  document.body.appendChild(element);

  // simulate user click (triggers download via Browser)
  element.click();

  // remove new href element from DOM
  document.body.removeChild(element);

  // show info in GUI element title
  document.getElementById("titleDownloadMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - Download initiated";

  // reset GUI element text area
  document.getElementById("textDownloadMapInfo").innerHTML = " ";
}

/* ----------
fetches the current meta data of the map
---------- */
function printmapsMapData() {
  // build printmaps 'map data' URL
  var printmapsMapDataRequestURL = printmapsURL + "metadata/" + printmapsMapID;

  // send printmaps request
  fetch(printmapsMapDataRequestURL, {
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
    },
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textMapDataInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleMapDataInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      // stringify JSON response
      var printmapsMapDataResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textMapDataInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsMapDataResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleMapDataInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textMapDataInfo").innerHTML = error;
    });
}

/* ----------
deletes all artifacts (map project) from server
---------- */
function printmapsDeleteMap() {
  // build printmaps 'delete map' URL
  var printmapsDeleteMapURL = printmapsURL + "delete/" + printmapsMapID;

  // send printmaps request
  fetch(printmapsDeleteMapURL, {
    method: "POST",
    headers: {
      "Content-Type": "application/vnd.api+json; charset=utf-8",
      Accept: "application/vnd.api+json; charset=utf-8",
    },
    body: "",
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textDeleteMapInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleDeleteMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      if (response.status === 204) {
        // add info to title of GUI element
        document.getElementById("titleDeleteMapInfo").innerHTML += " - map project deleted";

        // disable all buttons (beginning new map project requires page reload)
        document.getElementById("mapInputCreate").disabled = true;
        document.getElementById("mapActionCreate").disabled = true;
        document.getElementById("mapInputUpdate").disabled = true;
        document.getElementById("mapActionUpdate").disabled = true;
        document.getElementById("mapInputUpload").disabled = true;
        document.getElementById("mapActionUpload").disabled = true;
        document.getElementById("mapActionOrder").disabled = true;
        document.getElementById("mapActionStatus").disabled = true;
        document.getElementById("mapActionDownload").disabled = true;
        document.getElementById("mapActionData").disabled = true;
        document.getElementById("mapActionDelete").disabled = true;
        document.getElementById("mapActionFetch").disabled = true;
      }

      if (response.status !== 204) {
        return response.json();
      }
    })
    .then((data) => {
      if (data !== undefined) {
        var printmapsDeleteMapResponseData = JSON.stringify(data, undefined, 2);

        // show response data in text area of GUI element
        document.getElementById("textDeleteMapInfo").innerHTML +=
          "\n\nPrintmaps Response Data:\n" +
          printmapsDeleteMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
      }
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleDeleteMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textDeleteMapInfo").innerHTML = error;
    });
}

/* ----------
fetches the capabilities of the map service
---------- */
function printmapsFetchCapabilities() {
  // prerequisite: map ID exists
  if (printmapsURL === "") {
    // show error status in GUI element title
    document.getElementById("titleFetchCapabilitiesInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

    // show error info in GUI element text area
    document.getElementById("textFetchCapabilitiesInfo").innerHTML = "server URL missing";

    return;
  }

  // build printmaps 'fetch capabilities' URL
  var printmapsFetchCapabilitiesRequestURL = printmapsURL + "capabilities/service";

  // send printmaps request
  fetch(printmapsFetchCapabilitiesRequestURL, {
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
    },
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textFetchCapabilitiesInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleFetchCapabilitiesInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      // stringify JSON response
      var printmapsFetchCapabilitiesResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textFetchCapabilitiesInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsFetchCapabilitiesResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleFetchCapabilitiesInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error";

      // show error message in text area of GUI element
      document.getElementById("textFetchCapabilitiesInfo").innerHTML = error;
    });
}

/* ----------
action "button pressed" concerning 'load ID'
---------- */
document.getElementById("mapInputID").addEventListener("change", function selectedFileChanged() {
  if (this.files.length === 0) {
    // console.log("map create: nothing to do, no file selected");
    return;
  }

  const reader = new FileReader();
  reader.onload = function fileReadCompleted() {
    // save map ID
    printmapsMapID = reader.result;

    // show user feedback in GUI element title
    document.getElementById("titleIDInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "File successfully read";

    // reset GUI element text area
    document.getElementById("textIDInfo").innerHTML = " ";

    // verify ID
    const valid = isGUID(printmapsMapID);
    if (valid === false) {
      // show user feedback in GUI element title
      document.getElementById("titleIDInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + "map ID invalid - " + printmapsMapID;

      // reset GUI element text area
      document.getElementById("textIDInfo").innerHTML = " ";

      // reset map ID
      printmapsMapID = "";

      return;
    }
    console.log("ID is UUID4 = ", isGUID(printmapsMapID));

    // show user feedback in GUI element title
    document.getElementById("titleIDInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + printmapsMapID + " (ID)";

    // reset GUI element text area
    document.getElementById("textIDInfo").innerHTML = " ";

    // disable ID/Create buttons
    document.getElementById("mapInputID").disabled = true;
    document.getElementById("mapActionID").disabled = true;
    document.getElementById("mapInputCreate").disabled = true;
    document.getElementById("mapActionCreate").disabled = true;

    // enable update buttons
    document.getElementById("mapInputUpdate").disabled = false;
    document.getElementById("mapActionUpdate").disabled = false;
  };
  reader.readAsText(this.files[0]);
});

/* ----------
action "button pressed" concerning 'map create'
---------- */
document.getElementById("mapInputCreate").addEventListener("change", function selectedFileChanged() {
  if (this.files.length === 0) {
    // console.log("map create: nothing to do, no file selected");
    return;
  }

  // reset collapsible element
  if (printmapsMapYAML === "") {
    // reset GUI element title
    document.getElementById("titleCreateMapInfo").innerHTML = "Info";

    // reset GUI element text area
    document.getElementById("textCreateMapInfo").innerHTML = " ";
  }

  const reader = new FileReader();
  reader.onload = function fileReadCompleted() {
    // save map YAML
    printmapsMapYAML = reader.result;

    // show user feedback in GUI element title
    document.getElementById("titleCreateMapInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "File successfully read";

    // reset GUI element text area
    document.getElementById("textCreateMapInfo").innerHTML = " ";

    // parse YAML to JSON
    printmapsMapAttributes = {};
    try {
      printmapsMapAttributes = jsyaml.safeLoad(printmapsMapYAML);
    } catch (error) {
      // show parsing error in GUI element title
      document.getElementById("titleCreateMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + "Error parsing YAML map file";

      // reset GUI element text area
      document.getElementById("textCreateMapInfo").innerHTML = error;

      printmapsMapYAML = "";
      return;
    }

    // show user feedback in GUI element title
    document.getElementById("titleCreateMapInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "YAML map file successfully parsed";

    // reset GUI element text area
    document.getElementById("textCreateMapInfo").innerHTML = " ";
  };
  reader.readAsText(this.files[0]);
});

/* ----------
action "button pressed" concerning 'map update'
<input type="file" id="mapInputUpdate" accept=".yaml" onclick="this.value=null;">
The 'change' event only gets fired when something actually changes.
This isn't the case, if one tries to select the same file a second time.
The 'onclick' statement in 'input' destroys the previous file object,
and the 'change' event gets always fired.
---------- */
document.getElementById("mapInputUpdate").addEventListener("change", function selectedFileChanged() {
  if (this.files.length === 0) {
    // console.log("map update: nothing to do, no file selected");
    return;
  }

  const reader = new FileReader();
  reader.onload = function fileReadCompleted() {
    // save map YAML
    printmapsMapYAML = reader.result;

    // show user feedback in GUI element title
    document.getElementById("titleUpdateMapInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "File successfully read";

    // reset GUI element text area
    document.getElementById("textUpdateMapInfo").innerHTML = " ";

    // parse YAML to JSON
    printmapsMapAttributes = {};
    try {
      printmapsMapAttributes = jsyaml.safeLoad(printmapsMapYAML);
    } catch (error) {
      // show parsing error in GUI element title
      document.getElementById("titleUpdateMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + "Error parsing YAML map file";

      // show error in GUI element text area
      document.getElementById("textUpdateMapInfo").innerHTML = error;

      printmapsMapYAML = "";
      return;
    }

    // show user feedback in GUI element title
    document.getElementById("titleUpdateMapInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "YAML map file successfully parsed";

    // reset GUI element text area
    document.getElementById("textUpdateMapInfo").innerHTML = " ";
  };

  reader.readAsText(this.files[0]);
});

/* ----------
action "clicked" concerning all collapsible elements
---------- */
var coll = document.getElementsByClassName("collapsible");
var i;
for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}

/* ----------
verify that sting is valid UUID4
---------- */
function isGUID(stringToTest) {
  if (stringToTest[0] === "{") {
    stringToTest = stringToTest.substring(1, stringToTest.length - 1);
  }
  var regexGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/gi;

  return regexGuid.test(stringToTest);
}
