/*
Purpose:
- Browser Printmaps Client in Javascript.

Description:
- Requires Browser with HTML5 support.

Releases:
- v0.1.0 - 2020/07/24 : initial release
- v0.2.0 - 2020/07/27 : verify existence of mandatory attributes

Author:
- Klaus Tockloth

Copyright and license:
- Copyright (c) 2020 Klaus Tockloth
- MIT license

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the Software), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

The software is provided 'as is', without warranty of any kind, express or implied, including
but not limited to the warranties of merchantability, fitness for a particular purpose and
noninfringement. In no event shall the authors or copyright holders be liable for any claim,
damages or other liability, whether in an action of contract, tort or otherwise, arising from,
out of or in connection with the software or the use or other dealings in the software.

Contact (eMail):
- printmaps.service@gmail.com

Remarks:
- NN

Links:
- NN
*/

// global vars
var printmapsMapYAML = "";
var printmapsMapAttribues = {};
var printmapsURL = "";
var printmapsMapID = "";
var mapBuildPending = true;
var mapBuildSuccessful = false;

/* ----------
initialize YAML editor
---------- */
var editor = CodeMirror.fromTextArea(document.getElementById("editor"), {
  mode: "yaml",
  lineNumbers: true,
  lineWrapping: false,
  gutters: ["CodeMirror-lint-markers"],
  lint: true,
});
editor.setSize("100%", "40%");
cmResize(editor);

/* ----------
action "file select" concerning 'yaml edit'
---------- */
document.getElementById("mapInputEdit").addEventListener("change", function selectedFileChanged() {
  if (this.files.length === 0) {
    // console.log("map create: nothing to do, no file selected");
    return;
  }

  const reader = new FileReader();
  reader.onload = function fileReadCompleted() {
    // initialize editor
    editor.setValue(reader.result);

    // enable buttons
    document.getElementById("mapInputSave").disabled = false;
    document.getElementById("mapInputEditUpload").disabled = false;
    document.getElementById("mapActionEditUpload").disabled = false;
    document.getElementById("mapActionBuildMap").disabled = false;
  };
  reader.readAsText(this.files[0]);
});

/* ----------
action "file select" concerning 'load ID'
---------- */
document.getElementById("mapInputID").addEventListener("change", function selectedFileChanged() {
  if (this.files.length === 0) {
    // console.log("map create: nothing to do, no file selected");
    return;
  }

  const reader = new FileReader();
  reader.onload = function fileReadCompleted() {
    // save map ID
    printmapsMapID = reader.result;

    // show user feedback in GUI element title
    document.getElementById("titleIDInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "File successfully read";

    // reset GUI element text area
    document.getElementById("textIDInfo").innerHTML = " ";

    // verify ID
    const valid = isGUID(printmapsMapID);
    if (valid === false) {
      // show user feedback in GUI element title
      document.getElementById("titleIDInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + "map ID invalid - " + printmapsMapID;

      // reset GUI element text area
      document.getElementById("textIDInfo").innerHTML = " ";

      // reset map ID
      printmapsMapID = "";

      return;
    }
    console.log("ID is UUID4 = ", isGUID(printmapsMapID));

    // show user feedback in GUI element title
    document.getElementById("titleIDInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + printmapsMapID + " (ID)";

    // reset GUI element text area
    document.getElementById("textIDInfo").innerHTML = " ";

    // disable button
    document.getElementById("mapInputID").disabled = true;
  };
  reader.readAsText(this.files[0]);
});

/* ----------
saves YAML map definition file
---------- */
function printmapsSaveYAML() {
  var blob = new Blob([editor.getValue()], { type: "text/plain;charset=utf-8" });
  saveAs(blob, "map.yaml");
}

/* ----------
action "clicked" concerning all collapsible elements
---------- */
var coll = document.getElementsByClassName("collapsible");
var i;
for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}

/* ----------
upload user supplied file (only one per action)
---------- */
async function printmapsEditUploadFile(input) {
  console.log("Uploading user file");

  if (printmapsURL === "") {
    var ok = parseMapDefinition("titleEditUploadFileInfo", "textEditUploadFileInfo");
    if (!ok) {
      return;
    }
  }

  if (printmapsMapID === "") {
    ok = await createMapProject("titleEditUploadFileInfo", "textEditUploadFileInfo");
    if (!ok) {
      return;
    }
  }

  ok = await updateMapProject("titleEditUploadFileInfo", "textEditUploadFileInfo");
  if (!ok) {
    return;
  }

  // build printmaps 'upload file' URL
  var printmapsUploadURL = printmapsURL + "upload/" + printmapsMapID;

  var xhr = new XMLHttpRequest();

  // show file upload progress
  xhr.upload.onprogress = function (e) {
    // calculate upload progress
    var progress = (e.loaded * 100) / e.total;

    // show upload progress in title of GUI element
    document.getElementById("titleEditUploadFileInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "Upload progress " + Math.round(progress) + "%";
  };

  // show file upload end
  xhr.upload.onload = function (e) {
    // show upload status in title of GUI element
    document.getElementById("titleEditUploadFileInfo").innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "Upload done";
    document.getElementById("textEditUploadFileInfo").innerHTML = "";
  };

  // show upload status in title of GUI element
  document.getElementById("titleEditUploadFileInfo").innerHTML =
    new Date(Date.now()).toLocaleTimeString() + " - " + "Upload progress 0%";

  // start upload
  xhr.open("POST", printmapsUploadURL, true);
  xhr.send(new FormData(input.parentElement));
}

/* ----------
fetch current map data
---------- */
function printmapsMapData() {
  console.log("Fetching map data");

  // build printmaps 'map data' URL
  var printmapsMapDataRequestURL = printmapsURL + "metadata/" + printmapsMapID;

  // send printmaps request
  fetch(printmapsMapDataRequestURL, {
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
    },
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textMapDataInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleMapDataInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      // stringify JSON response
      var printmapsMapDataResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById("textMapDataInfo").innerHTML +=
        "\n\nPrintmaps Response Data:\n" +
        printmapsMapDataResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    })
    .catch((error) => {
      // show error status in title of GUI element
      document.getElementById("titleMapDataInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + "Error fetching map data";

      // show error message in text area of GUI element
      document.getElementById("textMapDataInfo").innerHTML = error;
    });
}

/* ----------
build the map (async: Printmaps requests are serialized with await)
---------- */
async function printmapsBuildMap() {
  console.log("Building map");

  var ok = parseMapDefinition("titleBuildMapInfo", "textBuildMapInfo");
  if (!ok) {
    return;
  }

  if (printmapsMapID === "") {
    ok = await createMapProject("titleBuildMapInfo", "textBuildMapInfo");
    if (!ok) {
      return;
    }
  }

  ok = await updateMapProject("titleBuildMapInfo", "textBuildMapInfo");
  if (!ok) {
    return;
  }

  ok = await placeMapOrder("titleBuildMapInfo", "textBuildMapInfo");
  if (!ok) {
    return;
  }

  ok = await fetchBuildStatus("titleBuildMapInfo", "textBuildMapInfo");
  if (!ok) {
    return;
  }
}

/* ----------
sleep n milliseconds (www.sitepoint.com/delay-sleep-pause-wait/)
---------- */
function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

/* ----------
parse map definition (YAML -> JSON)
---------- */
function parseMapDefinition(title, text) {
  console.log("Parsing map definition");

  // show action in GUI element title
  document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Parsing map definition ...";

  printmapsMapAttributes = {};
  try {
    printmapsMapAttributes = jsyaml.safeLoad(editor.getValue());
  } catch (error) {
    printmapsMapAttributes = {};
    // show parsing error in GUI element title
    document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error parsing map definition";
    // show error in GUI element text area
    document.getElementById(text).innerHTML = error;
    return false;
  }

  // verify existence of mandatory attributes
  // console.log("printmapsMapAttributes =", printmapsMapAttributes);
  var attributeMissing = false;
  var attributeMissingText = "";
  if (!printmapsMapAttributes.hasOwnProperty("ServiceURL")) {
    attributeMissingText = "ServiceURL";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("Fileformat")) {
    attributeMissingText = "Fileformat";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("Scale")) {
    attributeMissingText = "Scale";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("PrintWidth")) {
    attributeMissingText = "PrintWidth";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("PrintHeight")) {
    attributeMissingText = "PrintHeight";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("Latitude")) {
    attributeMissingText = "Latitude";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("Longitude")) {
    attributeMissingText = "Longitude";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("Style")) {
    attributeMissingText = "Style";
    attributeMissing = true;
  } else if (!printmapsMapAttributes.hasOwnProperty("Projection")) {
    attributeMissingText = "Projection";
    attributeMissing = true;
  }
  if (attributeMissing) {
    // show parsing error in GUI element title
    document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error missing attribute";
    // show error in GUI element text area
    document.getElementById(text).innerHTML = "Mandatory attribute '" + attributeMissingText + "' missing.";
    return false;
  }

  // save printmaps root URL
  printmapsURL = printmapsMapAttributes.ServiceURL;

  // show user feedback in GUI element title
  document.getElementById(title).innerHTML =
    new Date(Date.now()).toLocaleTimeString() + " - " + "map definition successfully parsed";

  return true;
}

/* ----------
create map project
---------- */
async function createMapProject(title, text) {
  console.log("Creating map project");

  // show action in GUI element title
  document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Creating map project ...";

  // reset GUI element text area
  document.getElementById(text).innerHTML = " ";

  // build printmaps request
  var printmapsObject = {};
  printmapsObject.Data = {};
  printmapsObject.Data.Type = "maps";
  printmapsObject.Data.ID = "";
  printmapsObject.Data.Attributes = {};
  printmapsObject.Data.Attributes = printmapsMapAttributes;

  // 'Projection' is always a string
  printmapsObject.Data.Attributes.Projection = printmapsObject.Data.Attributes.Projection.toString();

  // 'UserFiles' is an internal object only for Printmaps commandline client
  delete printmapsObject.Data.Attributes.UserFiles;

  // stringify JSON request
  var printmapsCreateMapRequestData = JSON.stringify(printmapsObject, undefined, 2);

  // build printmaps 'create map' URL
  var printmapsCreateMapURL = printmapsURL + "metadata";

  // send printmaps request
  var success = true;
  await fetch(printmapsCreateMapURL, {
    method: "POST",
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
      "Content-Type": "application/vnd.api+json; charset=utf-8",
    },
    body: printmapsCreateMapRequestData,
  })
    .then(function (response) {
      // show response status in GUI element text area
      document.getElementById(text).innerHTML = "Create Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in GUI element title
      document.getElementById(title).innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      if (data.hasOwnProperty("Data")) {
        // save map ID
        printmapsMapID = data.Data.ID;

        // show ID in title of GUI element
        document.getElementById("titleIDInfo").innerHTML =
          new Date(Date.now()).toLocaleTimeString() + " - " + printmapsMapID + " (ID)";

        // enable buttons
        document.getElementById("mapInputSave").disabled = false;
        document.getElementById("mapActionData").disabled = false;
        document.getElementById("mapActionDelete").disabled = false;
        document.getElementById("mapActionID").disabled = false;

        // disable button
        document.getElementById("mapInputID").disabled = true;
      } else {
        success = false;
      }

      // stringify JSON error response
      var printmapsCreateMapResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById(text).innerHTML +=
        "\n\nCreate Response Data:\n" +
        printmapsCreateMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    })
    .catch((error) => {
      // show error message in text area of GUI element
      document.getElementById("textBuildMapInfo").innerHTML = error;

      success = false;
    });

  // show user feedback in GUI element title
  if (success) {
    document.getElementById(title).innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "Map project successfully created";
  } else {
    document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Error creating map project";
  }

  return success;
}

/* ----------
update map project
---------- */
async function updateMapProject(title, text) {
  console.log("Updating map project");

  // show action in GUI element title
  document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Updating map project ...";

  // reset GUI element text area
  document.getElementById(text).innerHTML = " ";

  // build printmaps request
  var printmapsObject = {};
  printmapsObject.Data = {};
  printmapsObject.Data.Type = "maps";
  printmapsObject.Data.ID = printmapsMapID;
  printmapsObject.Data.Attributes = {};
  printmapsObject.Data.Attributes = printmapsMapAttributes;

  // 'Projection' is always a string
  printmapsObject.Data.Attributes.Projection = printmapsObject.Data.Attributes.Projection.toString();

  // 'UserFiles' is an internal object only for printmaps commandline client
  delete printmapsObject.Data.Attributes.UserFiles;

  // stringify JSON request
  var printmapsUpdateMapRequestData = JSON.stringify(printmapsObject, undefined, 2);

  // save printmaps root URL
  printmapsURL = printmapsObject.Data.Attributes.ServiceURL;

  // build printmaps 'create map' URL
  var printmapsUpdateMapURL = printmapsURL + "metadata/patch/";

  // send printmaps request
  var success = true;
  await fetch(printmapsUpdateMapURL, {
    method: "POST",
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
      "Content-Type": "application/vnd.api+json; charset=utf-8",
    },
    body: printmapsUpdateMapRequestData,
  })
    .then(function (response) {
      // show response status in GUI element text area
      document.getElementById(text).innerHTML = "Update Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in GUI element title
      document.getElementById(title).innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      if (data.hasOwnProperty("Data")) {
        // enable buttons
        document.getElementById("mapInputSave").disabled = false;
        document.getElementById("mapActionData").disabled = false;
        document.getElementById("mapActionDelete").disabled = false;
        document.getElementById("mapActionID").disabled = false;

        // disable button
        document.getElementById("mapInputID").disabled = true;
      } else {
        success = false;
      }

      // stringify JSON response
      var printmapsUpdateMapResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById(text).innerHTML +=
        "\n\nUpdate Response Data:\n" +
        printmapsUpdateMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    })
    .catch((error) => {
      // show error message in text area of GUI element
      document.getElementById(text).innerHTML += "\n\nError updating map project:\n" + error;

      success = false;
    });

  // show user feedback in GUI element title
  if (success) {
    document.getElementById(title).innerHTML =
      new Date(Date.now()).toLocaleTimeString() + " - " + "Map project successfully updated";
  }

  return success;
}

/* ----------
place map order
---------- */
async function placeMapOrder(title, text) {
  console.log("Placing map order");

  // show action in GUI element title
  document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Placing map order ...";

  // build printmaps request
  var printmapsObject = {};
  printmapsObject.Data = {};
  printmapsObject.Data.Type = "maps";
  printmapsObject.Data.ID = printmapsMapID;

  // stringify JSON request
  var printmapsOrderMapRequestData = JSON.stringify(printmapsObject, undefined, 2);

  // build printmaps 'order map' URL
  var printmapsOrderMapURL = printmapsURL + "mapfile";

  // send printmaps request
  var success = true;
  await fetch(printmapsOrderMapURL, {
    method: "POST",
    headers: {
      Accept: "application/vnd.api+json; charset=utf-8",
      "Content-Type": "application/vnd.api+json; charset=utf-8",
    },
    body: printmapsOrderMapRequestData,
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById(text).innerHTML = "Order Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById(title).innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      return response.json();
    })
    .then((data) => {
      if (!data.hasOwnProperty("Data")) {
        success = false;
      }

      var printmapsOrderMapResponseData = JSON.stringify(data, undefined, 2);

      // show response data in text area of GUI element
      document.getElementById(text).innerHTML +=
        "\n\nOrder Response Data:\n" +
        printmapsOrderMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

      // disable button
      document.getElementById("mapActionDownload").disabled = true;
    })
    .catch((error) => {
      // show error message in text area of GUI element
      document.getElementById(text).innerHTML += "\n\nError placing map order:\n" + error;

      success = false;
    });

  return success;
}

/* ----------
fetch build status
---------- */
async function fetchBuildStatus(title, text) {
  console.log("Fetching build status");

  // show action in GUI element title
  document.getElementById(title).innerHTML = new Date(Date.now()).toLocaleTimeString() + " - " + "Fetching build status ...";

  // build printmaps 'map status' URL
  var printmapsMapStatusRequestURL = printmapsURL + "mapstate/" + printmapsMapID;

  // reset build control flags
  mapBuildPending = true;
  mapBuildSuccessful = false;

  var success = true;
  var i = 0;
  while (mapBuildPending) {
    await sleep(3000);

    console.log("fetching status ...");
    // send printmaps request
    success = true;
    await fetch(printmapsMapStatusRequestURL, {
      headers: {
        Accept: "application/vnd.api+json; charset=utf-8",
      },
    })
      .then(function (response) {
        // show response status in text area of GUI element
        document.getElementById(text).innerHTML = "Status Response Status:\n" + response.status + " (" + response.statusText + ")";

        // show response status in title of GUI element
        document.getElementById(title).innerHTML =
          new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

        return response.json();
      })
      .then((data) => {
        // stringify JSON response
        var printmapsMapStatusResponseData = JSON.stringify(data, undefined, 2);

        // show response data in text area of GUI element
        document.getElementById(text).innerHTML +=
          "\n\nStatus Response Data:\n" +
          printmapsMapStatusResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

        if (!data.hasOwnProperty("Data")) {
          success = false;
          return;
        }

        // show map build / processing state in title of GUI element
        if (data.Data.Attributes.MapBuildMessage !== "") {
          document.getElementById("titleBuildMapInfo").innerHTML += " - " + data.Data.Attributes.MapBuildMessage;
        } else if (data.Data.Attributes.MapBuildCompleted !== "") {
          document.getElementById("titleBuildMapInfo").innerHTML += " - " + "map build completed";
        } else if (data.Data.Attributes.MapBuildStarted !== "") {
          document.getElementById("titleBuildMapInfo").innerHTML += " - " + "map build started " + ".".repeat(i);
        } else if (data.Data.Attributes.MapOrderSubmitted !== "") {
          document.getElementById("titleBuildMapInfo").innerHTML += " - " + "map build submitted";
        } else if (data.Data.Attributes.MapMetadataWritten !== "") {
          document.getElementById("titleBuildMapInfo").innerHTML += " - " + "map metadata written";
        } else {
          document.getElementById("titleBuildMapInfo").innerHTML += " - " + "unknown status";
        }

        if (data.Data.Attributes.MapBuildSuccessful === "yes") {
          mapBuildPending = false;
          mapBuildSuccessful = true;
          // enable download button
          document.getElementById("mapActionDownload").disabled = false;
        }
        if (data.Data.Attributes.MapBuildSuccessful === "no") {
          mapBuildPending = false;
          mapBuildSuccessful = false;
        }
      })
      .catch((error) => {
        // show error status in text of GUI element
        document.getElementById(text).innerHTML += "\n\nError fetching map status:\n" + error;

        success = false;
      });
    i++;
    if (!success) {
      return success;
    }
  }

  return success;
}

/* ----------
download map
---------- */
function printmapsDownloadMap() {
  console.log("Downloading map");

  // build printmaps 'download' URL
  var printmapsDownloadMap = printmapsURL + "mapfile/" + printmapsMapID;

  // build HTML5 href (download) element
  var element = document.createElement("a");
  element.setAttribute("href", printmapsDownloadMap);
  element.setAttribute("download", "");
  element.setAttribute("type", "application/zip");

  // append new href element to DOM
  document.body.appendChild(element);

  // simulate user click (triggers download via Browser)
  element.click();

  // remove new href element from DOM
  document.body.removeChild(element);

  // show info in GUI element title
  document.getElementById("titleDownloadMapInfo").innerHTML = new Date(Date.now()).toLocaleTimeString() + " - Download initiated";

  // reset GUI element text area
  document.getElementById("textDownloadMapInfo").innerHTML = " ";
}

/* ----------
delete map project from server
---------- */
function printmapsDeleteMap() {
  console.log("Deleting map project");

  // build printmaps 'delete map' URL
  var printmapsDeleteMapURL = printmapsURL + "delete/" + printmapsMapID;

  // send printmaps request
  fetch(printmapsDeleteMapURL, {
    method: "POST",
    headers: {
      "Content-Type": "application/vnd.api+json; charset=utf-8",
      Accept: "application/vnd.api+json; charset=utf-8",
    },
    body: "",
  })
    .then(function (response) {
      // show response status in text area of GUI element
      document.getElementById("textDeleteMapInfo").innerHTML =
        "Printmaps Response Status:\n" + response.status + " (" + response.statusText + ")";

      // show response status in title of GUI element
      document.getElementById("titleDeleteMapInfo").innerHTML =
        new Date(Date.now()).toLocaleTimeString() + " - " + response.status + " (" + response.statusText + ")";

      if (response.status === 204) {
        // add info to title of GUI element
        document.getElementById("titleDeleteMapInfo").innerHTML += " - map project deleted";

        // disable all buttons (beginning new map project requires page reload)
        document.getElementById("mapInputEdit").disabled = true;
        document.getElementById("mapInputSave").disabled = true;
        document.getElementById("mapInputEditUpload").disabled = true;
        document.getElementById("mapActionEditUpload").disabled = true;
        document.getElementById("mapActionBuildMap").disabled = true;
        document.getElementById("mapActionDownload").disabled = true;
        document.getElementById("mapInputID").disabled = true;
        document.getElementById("mapActionID").disabled = true;
        document.getElementById("mapActionData").disabled = true;
        document.getElementById("mapActionDelete").disabled = true;
      }

      if (response.status !== 204) {
        return response.json();
      }
    })
    .then((data) => {
      if (data !== undefined) {
        var printmapsDeleteMapResponseData = JSON.stringify(data, undefined, 2);

        // show response data in text area of GUI element
        document.getElementById("textDeleteMapInfo").innerHTML +=
          "\n\nPrintmaps Response Data:\n" +
          printmapsDeleteMapResponseData.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
      }
    })
    .catch((error) => {
      // show error status in text of GUI element
      document.getElementById("textDeleteMapInfo").innerHTML += "\n\nError deleting map project:\n" + error;
    });
}

/* ----------
save map ID to file
 ---------- */
function printmapsSaveID() {
  // write file
  var blob = new Blob([printmapsMapID], { type: "text/plain;charset=utf-8" });
  saveAs(blob, "map.id");

  // disable button
  document.getElementById("mapActionID").disabled = true;
}

/* ----------
verify that string is valid UUID4
---------- */
function isGUID(stringToTest) {
  if (stringToTest[0] === "{") {
    stringToTest = stringToTest.substring(1, stringToTest.length - 1);
  }
  var regexGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/gi;

  return regexGuid.test(stringToTest);
}
