/*
Purpose:
- Browser Printmaps Tools in Javascript.

Description:
- Requires Browser with HTML5 support.

Releases:
- v0.1.0 - 2020/07/11 : initial release

Author:
- Klaus Tockloth

Copyright and license:
- Copyright (c) 2020 Klaus Tockloth
- MIT license

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the Software), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

The software is provided 'as is', without warranty of any kind, express or implied, including
but not limited to the warranties of merchantability, fitness for a particular purpose and
noninfringement. In no event shall the authors or copyright holders be liable for any claim,
damages or other liability, whether in an action of contract, tort or otherwise, arising from,
out of or in connection with the software or the use or other dealings in the software.

Contact (eMail):
- printmaps.service@gmail.com

Remarks:
- NN

Links:
- NN
*/

/* ----------
calculates wkt passepartout from base values
---------- */
function calculatePassepartout() {
  var passepartout = document.getElementById("passepartoutForm");
  var width = parseFloat(passepartout.elements["passepartoutWidth"].value);
  var height = parseFloat(passepartout.elements["passepartoutHeight"].value);
  var left = parseFloat(passepartout.elements["passepartoutLeft"].value);
  var top = parseFloat(passepartout.elements["passepartoutTop"].value);
  var right = parseFloat(passepartout.elements["passepartoutRight"].value);
  var bottom = parseFloat(passepartout.elements["passepartoutBottom"].value);
  var filename = passepartout.elements["passepartoutFilename"].value;

  // console.log("width =", width);
  // console.log("height =", height);
  // console.log("left =", left);
  // console.log("top =", top);
  // console.log("right =", right);
  // console.log("bottom =", bottom);
  // console.log("filename =", filename);

  var result = "well-known-text (wkt) as rectangle with hole (frame) (probably what you want) ...\n";

  result += sprintf(
    "POLYGON((%.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f), (%.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f))\n",
    // outer
    0.0,
    0.0,
    0.0,
    height,
    width,
    height,
    width,
    0.0,
    0.0,
    0.0,
    // inner
    left,
    bottom,
    left,
    height - top,
    width - right,
    height - top,
    width - right,
    bottom,
    left,
    bottom
  );

  result += "\nwell-known-text (wkt) as rectangle outlines (outer and inner) ...\n";

  result += sprintf(
    "LINESTRING(%.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f)\n",
    0.0,
    0.0,
    0.0,
    height,
    width,
    height,
    width,
    0.0,
    0.0,
    0.0
  );

  result += sprintf(
    "LINESTRING(%.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f)\n",
    left,
    bottom,
    left,
    height - top,
    width - right,
    height - top,
    width - right,
    bottom,
    left,
    bottom
  );

  // write file
  var blob = new Blob([result], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filename);
}

/* ----------
calculates wkt rectangle from base values
---------- */
function calculateRectangle() {
  var rectangle = document.getElementById("rectangleForm");
  var x = parseFloat(rectangle.elements["rectangleX"].value);
  var y = parseFloat(rectangle.elements["rectangleY"].value);
  var width = parseFloat(rectangle.elements["rectangleWidth"].value);
  var height = parseFloat(rectangle.elements["rectangleHeight"].value);
  var filename = rectangle.elements["rectangleFilename"].value;

  // console.log("x =", x);
  // console.log("y =", y);
  // console.log("width =", width);
  // console.log("height =", height);
  // console.log("filename =", filename);

  var result = sprintf("well-known-text (wkt) for rectangular box ...\n");
  result += sprintf(
    "LINESTRING(%.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f, %.1f %.1f)\n",
    x,
    y,
    x,
    y + height,
    x + width,
    y + height,
    x + width,
    y,
    x,
    y
  );

  // write file
  var blob = new Blob([result], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filename);
}

/* ----------
calculates wkt crop marks from base values
---------- */
function calculateCropmarks() {
  var cropmarks = document.getElementById("cropmarksForm");
  var width = parseFloat(cropmarks.elements["cropmarksWidth"].value);
  var height = parseFloat(cropmarks.elements["cropmarksHeight"].value);
  var size = parseFloat(cropmarks.elements["cropmarksSize"].value);
  var filename = cropmarks.elements["cropmarksFilename"].value;

  // console.log("width =", width);
  // console.log("height =", height);
  // console.log("size =", size);
  // console.log("filename =", filename);

  var result = sprintf("well-known-text (wkt) for crop marks ...\n");

  result += sprintf(
    "MULTILINESTRING((%.1f %.1f, %.1f %.1f, %.1f %.1f), " +
      "(%.1f %.1f, %.1f %.1f, %.1f %.1f), " +
      "(%.1f %.1f, %.1f %.1f, %.1f %.1f), " +
      "(%.1f %.1f, %.1f %.1f, %.1f %.1f))\n",
    // lower left crop mark
    0.0 + size,
    0.0,
    0.0,
    0.0,
    0.0,
    0.0 + size,
    // upper left crop mark
    0.0 + size,
    height,
    0.0,
    height,
    0.0,
    height - size,
    // upper right crop mark
    width - size,
    height,
    width,
    height,
    width,
    height - size,
    // lower right crop mark
    width - size,
    0.0,
    width,
    0.0,
    width,
    0.0 + size
  );

  // write file
  var blob = new Blob([result], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filename);
}

/* ----------
creates lat/lon grid in geojson format
---------- */
function createLatLonGrid() {
  var latlonGrid = document.getElementById("latLonGridForm");
  var latmin = parseFloat(latlonGrid.elements["latLonGridLatMin"].value);
  var lonmin = parseFloat(latlonGrid.elements["latLonGridLonMin"].value);
  var latmax = parseFloat(latlonGrid.elements["latLonGridLatMax"].value);
  var lonmax = parseFloat(latlonGrid.elements["latLonGridLonMax"].value);
  var distance = parseFloat(latlonGrid.elements["latLonGridDistance"].value);
  var latGridFilename = latlonGrid.elements["latLonGridFilenameLat"].value;
  var lonGridFilename = latlonGrid.elements["latLonGridFilenameLon"].value;

  // console.log("latmin =", latmin);
  // console.log("lonmin =", lonmin);
  // console.log("latmax =", latmax);
  // console.log("lonmax =", lonmax);
  // console.log("distance =", distance);
  // console.log("latGridFilename =", latGridFilename);
  // console.log("lonGridFilename =", lonGridFilename);

  // derive coord label format from 'fractional digits of grid distance'
  var fractionalDigits = 0;
  var distanceString = latlonGrid.elements["latLonGridDistance"].value;
  var tmp = distanceString.split(".");
  if (tmp.length > 1) {
    fractionalDigits = tmp[1].length;
  }
  var coordLabelFormat = sprintf("%%.%df", fractionalDigits);
  // console.log("coordLabelFormat =", coordLabelFormat);

  // geojson object (latitude grid)
  var latgrid = {
    type: "FeatureCollection",
    features: [],
  };

  // latitude grid lines (order: longitude, latitude)
  var i = 0;
  for (lattemp = latmin; lattemp <= latmax; lattemp += distance) {
    // new feature
    latgrid.features[i] = {
      type: "Feature",
      geometry: { type: "LineString", coordinates: [] },
      properties: null,
    };
    // set data for new feature
    latgrid.features[i].geometry.coordinates[0] = [lonmin, lattemp];
    latgrid.features[i].geometry.coordinates[1] = [lonmax, lattemp];
    latgrid.features[i].properties = {
      name: sprintf(coordLabelFormat, lattemp),
    };
    i++;
  }

  // write file for latitude grid lines
  var resultLatGrid = JSON.stringify(latgrid, undefined, 2);
  var blob = new Blob([resultLatGrid], { type: "text/plain;charset=utf-8" });
  saveAs(blob, latGridFilename);

  // geojson object (longitude grid)
  var longrid = {
    type: "FeatureCollection",
    features: [],
  };

  // longitude grid lines (order: longitude, latitude)
  var i = 0;
  for (lontemp = lonmin; lontemp <= lonmax; lontemp += distance) {
    // new feature
    longrid.features[i] = {
      type: "Feature",
      geometry: { type: "LineString", coordinates: [] },
      properties: null,
    };
    // set data for new feature
    longrid.features[i].geometry.coordinates[0] = [lontemp, latmin];
    longrid.features[i].geometry.coordinates[1] = [lontemp, latmax];
    longrid.features[i].properties = {
      name: sprintf(coordLabelFormat, lontemp),
    };
    i++;
  }

  // write file for longitude grid lines
  var resultLonGrid = JSON.stringify(longrid, undefined, 2);
  var blob = new Blob([resultLonGrid], { type: "text/plain;charset=utf-8" });
  saveAs(blob, lonGridFilename);
}

/* ----------
creates utm grid in geojson format
---------- */
function createUtmGrid() {
  var utmGrid = document.getElementById("utmGridForm");
  var zoneNumber = utmGrid.elements["utmGridZoneNumber"].value;
  var zoneLetter = utmGrid.elements["utmGridZoneLetter"].value;
  var utmMinEasting = parseInt(utmGrid.elements["utmGridMinEasting"].value);
  var utmMinNorthing = parseInt(utmGrid.elements["utmGridMinNorthing"].value);
  var utmMaxEasting = parseInt(utmGrid.elements["utmGridMaxEasting"].value);
  var utmMaxNorthing = parseInt(utmGrid.elements["utmGridMaxNorthing"].value);
  var distance = parseInt(utmGrid.elements["utmGridDistance"].value);
  var filenameHorizontal = utmGrid.elements["utmGridFilenameHorizontal"].value;
  var filenameVertical = utmGrid.elements["utmGridFilenameVertical"].value;

  // console.log("zoneNumber =", zoneNumber);
  // console.log("zoneLetter =", zoneLetter);
  // console.log("utmMinEasting =", utmMinEasting);
  // console.log("utmMinNorthing =", utmMinNorthing);
  // console.log("utmMaxEasting =", utmMaxEasting);
  // console.log("utmMaxNorthing =", utmMaxNorthing);
  // console.log("distance =", distance);
  // console.log("filenameHorizontal =", filenameHorizontal);
  // console.log("filenameVertical =", filenameVertical);

  // projections
  var projUTM = "+proj=utm +zone=" + zoneNumber.trim() + zoneLetter.trim();
  var projLONGLAT = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

  // horizontal grid lines

  // geojson object
  var horizontalGridLines = {
    type: "FeatureCollection",
    features: [],
  };

  // calculate horizontal grid lines
  var lonLatStart = [];
  var lonLatEnd = [];
  var i = 0;
  for (utmNorthingTemp = utmMinNorthing; utmNorthingTemp <= utmMaxNorthing; utmNorthingTemp += distance) {
    // calculate line
    lonLatStart = proj4(projUTM, projLONGLAT, [utmMinEasting, utmNorthingTemp]);
    lonLatEnd = proj4(projUTM, projLONGLAT, [utmMaxEasting, utmNorthingTemp]);

    // new geojson feature
    horizontalGridLines.features[i] = {
      type: "Feature",
      geometry: { type: "LineString", coordinates: [] },
      properties: null,
    };

    // set data for new feature
    horizontalGridLines.features[i].geometry.coordinates[0] = lonLatStart;
    horizontalGridLines.features[i].geometry.coordinates[1] = lonLatEnd;
    horizontalGridLines.features[i].properties = {
      name: sprintf("easting %.0f", utmNorthingTemp),
    };
    i++;
  }

  // write file for horizontal grid lines
  var resultHorizontalGridLines = JSON.stringify(horizontalGridLines, undefined, 2);
  var blob = new Blob([resultHorizontalGridLines], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filenameHorizontal);

  // vertical grid lines

  // geojson object
  var verticalGridLines = {
    type: "FeatureCollection",
    features: [],
  };

  lonLatStart = [];
  lonLatEnd = [];
  i = 0;
  for (utmEastingTemp = utmMinEasting; utmEastingTemp <= utmMaxEasting; utmEastingTemp += distance) {
    // calculate line
    lonLatStart = proj4(projUTM, projLONGLAT, [utmEastingTemp, utmMinNorthing]);
    lonLatEnd = proj4(projUTM, projLONGLAT, [utmEastingTemp, utmMaxNorthing]);

    // new geojson feature
    verticalGridLines.features[i] = {
      type: "Feature",
      geometry: { type: "LineString", coordinates: [] },
      properties: null,
    };

    // set data for new feature
    verticalGridLines.features[i].geometry.coordinates[0] = lonLatStart;
    verticalGridLines.features[i].geometry.coordinates[1] = lonLatEnd;
    verticalGridLines.features[i].properties = {
      name: sprintf("%.0f northing", utmEastingTemp),
    };
    i++;
  }

  // write file for vertical grid lines
  var resultVerticalGridLines = JSON.stringify(verticalGridLines, undefined, 2);
  var blob = new Blob([resultVerticalGridLines], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filenameVertical);
}

/* ----------
creates bearing line in geojson format
---------- */
function createBearingLine() {
  var bearingLine = document.getElementById("bearingLineForm");
  var lat = parseFloat(bearingLine.elements["bearingLineLat"].value);
  var lon = parseFloat(bearingLine.elements["bearingLineLon"].value);
  var angle = parseFloat(bearingLine.elements["bearingLineAngle"].value);
  var length = parseFloat(bearingLine.elements["bearingLineLength"].value);
  var linelabel = bearingLine.elements["bearingLineLinelabel"].value;
  var filename = bearingLine.elements["bearingLineFilename"].value;

  // console.log("lat =", lat);
  // console.log("lon =", lon);
  // console.log("angle =", angle);
  // console.log("length =", length);
  // console.log("linelabel =", linelabel);
  // console.log("filename =", filename);

  // calculate end point of bearing line
  var geod = GeographicLib.Geodesic.WGS84;
  var r = geod.Direct(lat, lon, angle, length);

  var bearingline = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        geometry: {
          type: "LineString",
          coordinates: [],
        },
        properties: null,
      },
    ],
  };

  // set data for new feature
  bearingline.features[0].geometry.coordinates[0] = [lon, lat];
  // bearingline.features[0].geometry.coordinates[1] = [r.lon2.toFixed(10), r.lat2.toFixed(10)];
  bearingline.features[0].geometry.coordinates[1] = [r.lon2, r.lat2];
  bearingline.features[0].properties = { name: linelabel };

  // write file
  var result = JSON.stringify(bearingline, undefined, 2);
  var blob = new Blob([result], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filename);
}

/* ----------
creates latlon line (beeline) in geojson format
---------- */
function createLatLonLine() {
  var latLonLine = document.getElementById("latLonLineForm");
  var latstart = parseFloat(latLonLine.elements["latLonLineLatStart"].value);
  var lonstart = parseFloat(latLonLine.elements["latLonLineLonStart"].value);
  var latend = parseFloat(latLonLine.elements["latLonLineLatEnd"].value);
  var lonend = parseFloat(latLonLine.elements["latLonLineLonEnd"].value);
  var linelabel = latLonLine.elements["latLonLinelabel"].value;
  var filename = latLonLine.elements["latLonLineFilename"].value;

  // console.log("latstart =", latstart);
  // console.log("lonstart =", lonstart);
  // console.log("latend =", latend);
  // console.log("lonend =", lonend);
  // console.log("linelabel =", linelabel);
  // console.log("filename =", filename);

  var latlonline = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        geometry: {
          type: "LineString",
          coordinates: [],
        },
        properties: null,
      },
    ],
  };

  // set data for new feature
  latlonline.features[0].geometry.coordinates[0] = [lonstart, latstart];
  latlonline.features[0].geometry.coordinates[1] = [lonend, latend];
  latlonline.features[0].properties = { name: linelabel };

  // write file
  var result = JSON.stringify(latlonline, undefined, 2);
  var blob = new Blob([result], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filename);
}

/* ----------
creates utm line (scalebar) in geojson format
---------- */
function createUtmLine() {
  var utmLine = document.getElementById("utmLineForm");
  var zoneNumber = utmLine.elements["utmLineZoneNumber"].value;
  var zoneLetter = utmLine.elements["utmLineZoneLetter"].value;
  var utmStartEasting = parseInt(utmLine.elements["utmLineStartEasting"].value);
  var utmStartNorthing = parseInt(utmLine.elements["utmLineStartNorthing"].value);
  var utmEndEasting = parseInt(utmLine.elements["utmLineEndEasting"].value);
  var utmEndNorthing = parseInt(utmLine.elements["utmLineEndNorthing"].value);
  var linelabel = utmLine.elements["utmLinelabel"].value;
  var filename = utmLine.elements["utmLineFilename"].value;

  // console.log("zoneNumber =", zoneNumber);
  // console.log("zoneLetter =", zoneLetter);
  // console.log("utmStartEasting =", utmStartEasting);
  // console.log("utmStartNorthing =", utmStartNorthing);
  // console.log("utmEndEasting =", utmEndEasting);
  // console.log("utmEndNorthing =", utmEndNorthing);
  // console.log("linelabel =", linelabel);
  // console.log("filename =", filename);

  // projections
  var projUTM = "+proj=utm +zone=" + zoneNumber.trim() + zoneLetter.trim();
  var projLONGLAT = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

  // start point: UTM -> latlon
  var lonlatStart = proj4(projUTM, projLONGLAT, [utmStartEasting, utmStartNorthing]);
  // console.log("lonlatStart =", lonlatStart);

  // end point: UTM -> latlon
  var lonlatEnd = proj4(projUTM, projLONGLAT, [utmEndEasting, utmEndNorthing]);
  // console.log("lonlatEnd =", lonlatEnd);

  var utmline = {
    type: "FeatureCollection",
    features: [
      {
        type: "Feature",
        geometry: {
          type: "LineString",
          coordinates: [],
        },
        properties: null,
      },
    ],
  };

  // set data for new feature
  utmline.features[0].geometry.coordinates[0] = lonlatStart;
  utmline.features[0].geometry.coordinates[1] = lonlatEnd;
  utmline.features[0].properties = { name: linelabel };

  // write file
  var result = JSON.stringify(utmline, undefined, 2);
  var blob = new Blob([result], { type: "text/plain;charset=utf-8" });
  saveAs(blob, filename);
}

/* ----------
action "show dialog" for passepartout
---------- */
(function () {
  var dialog = document.getElementById("passepartoutDialog");
  var form = document.getElementById("passepartoutForm");

  document.getElementById("passepartoutShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("passepartoutExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", calculatePassepartout, false);
})();

/* ----------
action "show dialog" for rectangle
---------- */
(function () {
  var dialog = document.getElementById("rectangleDialog");
  var form = document.getElementById("rectangleForm");

  document.getElementById("rectangleShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("rectangleExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", calculateRectangle, false);
})();

/* ----------
action "show dialog" for crop marks
---------- */
(function () {
  var dialog = document.getElementById("cropmarksDialog");
  var form = document.getElementById("cropmarksForm");

  document.getElementById("cropmarksShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("cropmarksExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", calculateCropmarks, false);
})();

/* ----------
action "show dialog" for latlon grid
---------- */
(function () {
  var dialog = document.getElementById("latLonGridDialog");
  var form = document.getElementById("latLonGridForm");

  document.getElementById("latLonGridShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("latLonGridExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", createLatLonGrid, false);
})();

/* ----------
action "show dialog" for utm grid
---------- */
(function () {
  var dialog = document.getElementById("utmGridDialog");
  var form = document.getElementById("utmGridForm");

  document.getElementById("utmGridShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("utmGridExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", createUtmGrid, false);
})();

/* ----------
action "show dialog" for bearing line
---------- */
(function () {
  var dialog = document.getElementById("bearingLineDialog");
  var form = document.getElementById("bearingLineForm");

  document.getElementById("bearingLineShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("bearingLineExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", createBearingLine, false);
})();

/* ----------
action "show dialog" for latlon line
---------- */
(function () {
  var dialog = document.getElementById("latLonLineDialog");
  var form = document.getElementById("latLonLineForm");

  document.getElementById("latLonLineShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("latLonLineExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", createLatLonLine, false);
})();

/* ----------
action "show dialog" for utm line
---------- */
(function () {
  var dialog = document.getElementById("utmLineDialog");
  var form = document.getElementById("utmLineForm");

  document.getElementById("utmLineShowDialog").onclick = function () {
    dialog.showModal();
  };

  document.getElementById("utmLineExitDialog").onclick = function () {
    dialog.close();
  };

  form.addEventListener("submit", createUtmLine, false);
})();

/* ----------
action "clicked" concerning all collapsible elements
---------- */
var coll = document.getElementsByClassName("collapsible");
var i;
for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function () {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    }
  });
}
