#!/bin/sh

# ------------------------------------
# Function:
# - verifies all webpages
#
# Version:
# - v0.1.0 - 2019/05/02 initial release
# - v0.2.0 - 2020/07/05 page added
# - v0.3.0 - 2020/07/09 page added
#
# Prerequisites:
# - tidy-html5 installed (v5.6)
# ------------------------------------

# set -o xtrace
set -o verbose

tidy -version

tidy -config tidy-config.txt -errors -quiet de/beispiele.html
tidy -config tidy-config.txt -errors -quiet de/client.html
tidy -config tidy-config.txt -errors -quiet de/impressum.html
tidy -config tidy-config.txt -errors -quiet de/index.html
tidy -config tidy-config.txt -errors -quiet de/nutzung.html
tidy -config tidy-config.txt -errors -quiet de/server.html
tidy -config tidy-config.txt -errors -quiet de/gui.html
tidy -config tidy-config.txt -errors -quiet de/tools.html

tidy -config tidy-config.txt -errors -quiet en/beispiele.html
tidy -config tidy-config.txt -errors -quiet en/client.html
tidy -config tidy-config.txt -errors -quiet en/impressum.html
tidy -config tidy-config.txt -errors -quiet en/index.html
tidy -config tidy-config.txt -errors -quiet en/nutzung.html
tidy -config tidy-config.txt -errors -quiet en/server.html
tidy -config tidy-config.txt -errors -quiet en/gui.html
tidy -config tidy-config.txt -errors -quiet en/tools.html
