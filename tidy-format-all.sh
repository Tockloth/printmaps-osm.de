#!/bin/sh

# ------------------------------------
# Function:
# - formats all webpages
#
# Version:
# - v0.1.0 - 2019/05/03 initial release
# - v0.2.0 - 2020/07/05 page added
# - v0.2.0 - 2020/07/09 page added
#
# Prerequisites:
# - tidy-html5 installed (v5.6)
# ------------------------------------

# set -o xtrace
set -o verbose

tidy -version

tidy -config tidy-config.txt -quiet -modify de/beispiele.html
tidy -config tidy-config.txt -quiet -modify de/client.html
tidy -config tidy-config.txt -quiet -modify de/impressum.html
tidy -config tidy-config.txt -quiet -modify de/index.html
tidy -config tidy-config.txt -quiet -modify de/nutzung.html
tidy -config tidy-config.txt -quiet -modify de/server.html
tidy -config tidy-config.txt -quiet -modify de/gui.html
tidy -config tidy-config.txt -quiet -modify de/tools.html

tidy -config tidy-config.txt -quiet -modify en/beispiele.html
tidy -config tidy-config.txt -quiet -modify en/client.html
tidy -config tidy-config.txt -quiet -modify en/impressum.html
tidy -config tidy-config.txt -quiet -modify en/index.html
tidy -config tidy-config.txt -quiet -modify en/nutzung.html
tidy -config tidy-config.txt -quiet -modify en/server.html
tidy -config tidy-config.txt -quiet -modify en/gui.html
tidy -config tidy-config.txt -quiet -modify en/tools.html
