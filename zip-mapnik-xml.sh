#!/bin/sh

# ------------------------------------
# Function:
# - zip all mapnik xml files
#
# Version:
# - v0.1.0 - 2020-06-30 initiale Version
# - v0.2.0 - 2021-04-10 Pfade geändert
# - v0.3.0 - 2022-08-14 Pfade geändert
# ------------------------------------

# set -o xtrace
set -o verbose

zip osm-carto.xml.zip ./printstyles/osm-carto-5.6.1/mapnik.xml
zip osm-carto-ele20.xml.zip ./printstyles/osm-carto-ele20-5.6.1/mapnik.xml
zip osm-carto-mono.xml.zip ./printstyles/osm-carto-mono-5.6.1/mapnik.xml
zip schwarzplan.xml.zip ./printstyles/schwarzplan-1.1.0/mapnik.xml
zip schwarzplan+.xml.zip ./printstyles/schwarzplan+-1.12.0/mapnik.xml
zip raster10.xml.zip ./printstyles/raster10-1.0.0/mapnik.xml
zip transparent.xml.zip ./printstyles/transparent-1.0.0/mapnik.xml
